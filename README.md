# Frontend test

Welcome at Fiyo. This test is created to get a general overview of your frontend capabilities. Please spend a maximum of one hour to this test.

Create a merge request to this repository when finished.

## Overview

### Goal
In the next hour, you're going to create a todo app. This app should contain the following functionality:
- the user needs to be able to see an overview of all todos
- the user needs to be able to create a new todo
- the user needs to be able to change the content of an existing todo
- the user needs to be able to delete a todo
- the user needs to be able to mark a todo as done

### Grading
You will be graded based on the implementation of the application, as well as the **user experience**.

### Getting started
A backend has already been set up for you using [json-server](https://github.com/typicode/json-server). all you have to do is to run the following command to start up the backend:
```sh
docker-compose up -d
```

We also pre-installed and set up some packages, so you can use them from the get-go. You are allowed to install packages as well.
You're not required to use any of the pre-installed packages, but it might save you some time to use them.
We installed react-redux for you. However, it's not mandatory to use redux.

### Documentation links
Here are some links to the documentation of these packages, so you don't have to look them up yourself.
- [Semantic UI React](https://react.semantic-ui.com/)
- [Styled Components](https://styled-components.com/docs)
- [React Redux](https://redux.js.org/introduction/getting-started)
- [axios](https://github.com/axios/axios) [[cheatsheet]](https://kapeli.com/cheat_sheets/Axios.docset/Contents/Resources/Documents/index)