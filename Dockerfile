FROM node:14-alpine

RUN yarn global add json-server
COPY db.json db.json

EXPOSE 5000
CMD [ "json-server", "--host=0.0.0.0", "--port=5000", "--nc", "db.json" ]