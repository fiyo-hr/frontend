import { Reducer } from '@reduxjs/toolkit';

export const storeReducer: Reducer = (state = {}, action) => {
  switch (action.type) {
    default:
      return state;
  }
}
