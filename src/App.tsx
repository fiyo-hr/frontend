import React from 'react';
import AppContainer from './AppContainer';
import { Container, Header, Card } from 'semantic-ui-react'

const App = () => (
  <div className="App">
    <AppContainer>
      <Container text textAlign='center'>
        <Header as='h1'>Front-End Excercise</Header>
        <Card centered>
          <Card.Content>
            Please read the README for more information.
          </Card.Content>
        </Card>
      </Container>
    </AppContainer>
  </div>
);

export default App;
