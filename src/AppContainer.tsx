import styled from 'styled-components';

const AppContainer = styled.div`
  margin-top: 8rem;
`;

export default AppContainer;
