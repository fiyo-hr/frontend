import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { Provider } from 'react-redux';
import { storeReducer } from './store';
import 'semantic-ui-css/semantic.min.css'
import { createStore } from '@reduxjs/toolkit';
import { composeWithDevTools } from "redux-devtools-extension";

const store = createStore(storeReducer, composeWithDevTools());

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
